package com.etnetera.hr.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "js_framework_version")
@Data
@NoArgsConstructor @AllArgsConstructor
public class JavaScriptFrameworkVersion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long frameworkId;

    @Column(nullable = false, length = 10)
    private String version;

    private LocalDate deprecationDate;
}

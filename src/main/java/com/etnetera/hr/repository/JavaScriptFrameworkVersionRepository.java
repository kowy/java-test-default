package com.etnetera.hr.repository;

import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Spring data repository interface used for accessing the data in database.
 * 
 * @author Etnetera
 *
 */
public interface JavaScriptFrameworkVersionRepository extends CrudRepository<JavaScriptFrameworkVersion, Long> {

    @Query(value = "SELECT j FROM JavaScriptFrameworkVersion j WHERE j.frameworkId IN (?1) AND j.deprecationDate >= ?2")
    List<JavaScriptFrameworkVersion> findActiveVersions(List<Long> frameworks, LocalDate now);

    List<JavaScriptFrameworkVersion> findByFrameworkId(Long frameworkId);

    Optional<JavaScriptFrameworkVersion> findByFrameworkIdAndVersion(Long frameworkId, String version);
}

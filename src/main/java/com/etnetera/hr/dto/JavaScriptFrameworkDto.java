package com.etnetera.hr.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor @AllArgsConstructor
public class JavaScriptFrameworkDto {
    private Long id;
    private String name;
    private BigDecimal hypeLevel;
    private String version;
    private LocalDate deprecationDate;
}

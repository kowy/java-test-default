package com.etnetera.hr.service;

import com.etnetera.hr.dto.JavaScriptFrameworkDto;
import com.etnetera.hr.exception.NotFoundException;

public interface JavaScriptService {

    Iterable<JavaScriptFrameworkDto> listFrameworks(String name, Boolean active);

    Long addFramework(JavaScriptFrameworkDto framework);

    Boolean editFramework(Long id, JavaScriptFrameworkDto framework) throws NotFoundException;

    Boolean deleteFramework(Long id) throws NotFoundException;
}

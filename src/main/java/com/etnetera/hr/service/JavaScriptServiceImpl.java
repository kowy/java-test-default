package com.etnetera.hr.service;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import com.etnetera.hr.dto.JavaScriptFrameworkDto;
import com.etnetera.hr.exception.NotFoundException;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.repository.JavaScriptFrameworkVersionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class JavaScriptServiceImpl implements JavaScriptService {

    private final JavaScriptFrameworkRepository frameworkRepository;
    private final JavaScriptFrameworkVersionRepository versionRepository;

    @Autowired
    public JavaScriptServiceImpl(JavaScriptFrameworkRepository frameworkRepository, JavaScriptFrameworkVersionRepository versionRepository) {
        this.frameworkRepository = frameworkRepository;
        this.versionRepository = versionRepository;
    }

    @Override
    public Iterable<JavaScriptFrameworkDto> listFrameworks(String name, Boolean active) {
        final List<JavaScriptFramework> fwList = frameworkRepository.findByNameStartsWith(name);
        final List<Long> fwIdList = fwList.stream().map(JavaScriptFramework::getId).collect(Collectors.toList());

        List<JavaScriptFrameworkVersion> versionList;
        if (active) {
            versionList = versionRepository.findActiveVersions(fwIdList, LocalDate.now());
        } else {
            // předpokládám, že žádný FW nezanikl před 1.1.1980
            versionList = versionRepository.findActiveVersions(fwIdList, LocalDate.ofEpochDay(0));
        }

        final List<JavaScriptFrameworkDto> result = fwList.stream()
                .map(fw -> {
                    // ke každému FW připojí jeho verze a přemapuje to na REST objekt
                    return versionList.stream()
                            .filter(version -> version.getFrameworkId().equals(fw.getId()))
                            .map(version -> mapEntity2dto(fw, version));
                })
                .flatMap(dto -> dto)
                .collect(Collectors.toList());

        return result;
    }

    @Override
    @Transactional
    public Long addFramework(JavaScriptFrameworkDto framework) {
        JavaScriptFramework fwEntity = frameworkRepository.findByName(framework.getName()).orElse(new JavaScriptFramework());
        mapDto2EntityFw(framework, fwEntity);
        fwEntity = frameworkRepository.save(fwEntity);

        JavaScriptFrameworkVersion versionEntity = versionRepository
                .findByFrameworkIdAndVersion(fwEntity.getId(), framework.getVersion())
                .orElse(new JavaScriptFrameworkVersion());
        mapDto2EntityVersion(framework, versionEntity, fwEntity.getId());
        versionEntity = versionRepository.save(versionEntity);

        return versionEntity.getId();
    }

    @Override
    @Transactional
    public Boolean editFramework(Long id, JavaScriptFrameworkDto framework) throws NotFoundException {
        final JavaScriptFrameworkVersion version = versionRepository.findById(id).orElseThrow(NotFoundException::new);
        mapDto2EntityVersion(framework, version, version.getFrameworkId());
        versionRepository.save(version);

        JavaScriptFramework fw = frameworkRepository.findById(version.getFrameworkId()).orElseThrow(NotFoundException::new);
        mapDto2EntityFw(framework, fw);
        frameworkRepository.save(fw);

        return true;
    }

    @Override
    public Boolean deleteFramework(Long id) throws NotFoundException {
        final JavaScriptFrameworkVersion version = versionRepository.findById(id).orElseThrow(NotFoundException::new);
        final Long frameworkId = version.getFrameworkId();
        versionRepository.delete(version);

        final int versionCount = versionRepository.findByFrameworkId(frameworkId).size();
        if (versionCount < 1) {
            // odstraněna poslední verze => odstraň celou instanci Framework
            frameworkRepository.findById(frameworkId)
                    .ifPresent(frameworkRepository::delete);
        }

        return true;
    }

    private JavaScriptFrameworkDto mapEntity2dto(JavaScriptFramework framework, JavaScriptFrameworkVersion version) {
        final JavaScriptFrameworkDto result = new JavaScriptFrameworkDto();
        BeanUtils.copyProperties(framework, result);
        BeanUtils.copyProperties(version, result);

        return result;
    }

    private void mapDto2EntityFw(JavaScriptFrameworkDto dto, JavaScriptFramework entity) {
        entity.setName(dto.getName());
        entity.setHypeLevel(dto.getHypeLevel());
    }

    private void mapDto2EntityVersion(JavaScriptFrameworkDto dto, JavaScriptFrameworkVersion entity, Long frameworkId) {
        entity.setVersion(dto.getVersion());
        entity.setDeprecationDate(dto.getDeprecationDate());
        entity.setFrameworkId(frameworkId);
    }
}

package com.etnetera.hr.controller;

import com.etnetera.hr.dto.JavaScriptFrameworkDto;
import com.etnetera.hr.dto.StatusMessageDto;
import com.etnetera.hr.exception.NotFoundException;
import com.etnetera.hr.service.JavaScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Simple REST controller for accessing application logic.
 *  
 * @author Etnetera
 *
 */
@RestController
public class JavaScriptFrameworkController {

	private final JavaScriptService jsService;

	@Autowired
	public JavaScriptFrameworkController(JavaScriptService jsService) {
		this.jsService = jsService;
	}

	@GetMapping("/frameworks")
	public Iterable<JavaScriptFrameworkDto> frameworks(@RequestParam(name = "name") String name,
													@RequestParam(name = "active") Boolean active) {
		return jsService.listFrameworks(name, active);
	}

	@PostMapping("/frameworks")
	public StatusMessageDto addFramework(@RequestBody JavaScriptFrameworkDto framework) {
		final Long id = jsService.addFramework(framework);

		return new StatusMessageDto(id.toString());
	}

	@PostMapping("/frameworks/{id}")
	public StatusMessageDto editFramework(@PathVariable(name = "id") Long id,
											@RequestBody JavaScriptFrameworkDto framework) throws Exception {
		try {
			final Boolean result = jsService.editFramework(id, framework);
			return new StatusMessageDto(result ? "ok" : "fail");
		} catch (NotFoundException e) {
			// log.info("Pokus o editaci neexistujícího frameworku {}", id);
			throw new Exception(String.format("Framework ID %s neexistuje", id));
		}
	}

	@DeleteMapping("/frameworks/{id}")
	public StatusMessageDto deleteFramework(@PathVariable(name = "id") Long id) throws Exception {
		try {
			final Boolean result = jsService.deleteFramework(id);
			return new StatusMessageDto(result ? "ok" : "fail");
		} catch (NotFoundException e) {
			// log.info("Pokus o smazání neexistujícího frameworku {}", id);
			throw new Exception(String.format("Framework ID %s neexistuje", id));
		}

	}
}

package com.etnetera.hr;

import java.util.ArrayList;
import java.util.List;

public class TestUtils {
    public static <T> List<T> iterableToList(Iterable<T> iterator) {
        List<T> list = new ArrayList<>();
        iterator.forEach(list::add);
        return list;
    }
}

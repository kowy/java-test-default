package com.etnetera.hr.service;

import com.etnetera.hr.TestUtils;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import com.etnetera.hr.dto.JavaScriptFrameworkDto;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.repository.JavaScriptFrameworkVersionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * Jiný způsob testů s využitím Mockování
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class JavaScriptServiceImplTest {

    public static final BigDecimal PRECISION = new BigDecimal(0.005);
    private JavaScriptServiceImpl service;
    @Mock private JavaScriptFrameworkRepository frameworkRepository;
    @Mock private JavaScriptFrameworkVersionRepository versionRepository;
    @Captor private ArgumentCaptor<List<Long>> listLongCaptor;


    @Before
    public void setUp() throws Exception {
        service = new JavaScriptServiceImpl(frameworkRepository, versionRepository);
    }

    @Test
    public void listFrameworks() {
        when(frameworkRepository.findByNameStartsWith(anyString())).thenReturn(
                Collections.singletonList(new JavaScriptFramework(19L, "Angular", new BigDecimal(9.1)))
        );
        when(versionRepository.findActiveVersions(anyObject(), anyObject())).thenReturn(
                Collections.singletonList(new JavaScriptFrameworkVersion(199L, 19L, "1.0", LocalDate.of(2015, 4, 25)))
        );
        final List<JavaScriptFrameworkDto> angular = TestUtils.iterableToList(service.listFrameworks("Angular", true));
        assertThat("Počet položek", angular.size(), is(1));
        assertThat(angular.get(0).getId(), is(199L));
        assertThat(angular.get(0).getName(), is("Angular"));
        assertThat(angular.get(0).getVersion(), is("1.0"));
        assertThat(angular.get(0).getHypeLevel(), is(closeTo(new BigDecimal(9.1), PRECISION)));
        assertThat(angular.get(0).getDeprecationDate(), is(LocalDate.of(2015, 4, 25)));

        verify(versionRepository, only()).findActiveVersions(listLongCaptor.capture(), anyObject());
        assertThat("IDčka zachycená captorem", listLongCaptor.getValue(), contains(19L));
    }
}
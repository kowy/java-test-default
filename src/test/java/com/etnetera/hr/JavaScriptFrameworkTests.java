package com.etnetera.hr;

import com.etnetera.hr.controller.JavaScriptFrameworkController;
import com.etnetera.hr.dto.JavaScriptFrameworkDto;
import com.etnetera.hr.dto.StatusMessageDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;


/**
 * Class used for Spring Boot/MVC based tests.
 * 
 * @author Etnetera
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = {"classpath:setup.sql"})
public class JavaScriptFrameworkTests {
    private static final Logger LOG = LoggerFactory.getLogger(JavaScriptFrameworkTests.class);
    public static final BigDecimal PRECISION = new BigDecimal(0.005);

    @Autowired
    private JavaScriptFrameworkController controller;

	@Test
    public void testAddVersion() {
        final JavaScriptFrameworkDto angular = new JavaScriptFrameworkDto(0L, "Angular", new BigDecimal(9.0), "2.0", LocalDate.of(2017, 5, 1));
        final StatusMessageDto status = controller.addFramework(angular);
        final Long id = Long.valueOf(status.getMessage());

        final List<JavaScriptFrameworkDto> frameworks = TestUtils.iterableToList(controller.frameworks("Angular", false));
        assertThat(frameworks.size(), is(2));
        assertThat(frameworks.stream().map(JavaScriptFrameworkDto::getId).collect(Collectors.toList()), containsInAnyOrder(100L, id));
        assertThat(frameworks.stream().map(JavaScriptFrameworkDto::getName).collect(Collectors.toList()), contains("Angular", "Angular"));
        assertThat(frameworks.stream().map(JavaScriptFrameworkDto::getVersion).collect(Collectors.toList()), containsInAnyOrder("1.0", "2.0"));
    }

    @Test
    public void testAddFramework() {
        final JavaScriptFrameworkDto vue = new JavaScriptFrameworkDto(0L, "Vue.js", new BigDecimal(7.1), "1.0", LocalDate.of(2017, 5, 1));
        final StatusMessageDto status = controller.addFramework(vue);
        final Long id = Long.valueOf(status.getMessage());

        final List<JavaScriptFrameworkDto> frameworks = TestUtils.iterableToList(controller.frameworks("Vue.js", false));

        assertThat(frameworks.size(), is(1));
        assertThat(frameworks.get(0).getId(), is(id));
        assertThat(frameworks.get(0).getHypeLevel(), is(closeTo(new BigDecimal(7.1), PRECISION)));
        assertThat(frameworks.get(0).getName(), is("Vue.js"));
        assertThat(frameworks.get(0).getVersion(), is("1.0"));
    }

    @Test
    public void testEditFramework() throws Exception {
        final JavaScriptFrameworkDto angular = new JavaScriptFrameworkDto(0L, "Angular", new BigDecimal(9.9), "2.0", LocalDate.of(2017, 5, 1));
        final StatusMessageDto status = controller.editFramework(100L, angular);
        assertThat(status.getMessage(), is("ok"));

        final List<JavaScriptFrameworkDto> frameworks = TestUtils.iterableToList(controller.frameworks("Angular", false));
        assertThat(frameworks.size(), is(1));
        assertThat(frameworks.get(0).getHypeLevel(), is(closeTo(new BigDecimal(9.9), PRECISION)));
    }

    @Test
    public void testDeleteFramework() throws Exception {
	    // příprava dat
        final JavaScriptFrameworkDto angular = new JavaScriptFrameworkDto(0L, "Angular", new BigDecimal(9.0), "2.0", LocalDate.of(2017, 5, 1));
        final StatusMessageDto status = controller.addFramework(angular);
        final Long id = Long.valueOf(status.getMessage());
        // ověř, že jsou tam 2
        final List<JavaScriptFrameworkDto> frameworks0 = TestUtils.iterableToList(controller.frameworks("Angular", false));
        assertThat(frameworks0.size(), is(2));

        // mažu jeden
        final StatusMessageDto status1 = controller.deleteFramework(100L);
        assertThat(status1.getMessage(), is("ok"));
        final List<JavaScriptFrameworkDto> frameworks1 = TestUtils.iterableToList(controller.frameworks("Angular", false));
        assertThat(frameworks1.size(), is(1));

        // mažu poslední
        final StatusMessageDto status2 = controller.deleteFramework(id);
        assertThat(status2.getMessage(), is("ok"));
        final List<JavaScriptFrameworkDto> frameworks2 = TestUtils.iterableToList(controller.frameworks("Angular", false));
        assertThat(frameworks2.size(), is(0));

        // smazaný už nejde smazat
        boolean thrown = false;
        try {
            controller.deleteFramework(id);
        } catch (Exception ex) {
            thrown = true;
        }
        assertThat("deleteFramework nevyhodil výjimku při mazání neexistující položky", thrown, is(true));
    }
}

DELETE FROM js_framework_version;
DELETE FROM js_framework;

INSERT INTO js_framework (id, name, hype_level)
  VALUES (50, 'Angular', 9.0);

INSERT INTO js_framework_version (id, framework_id, version, deprecation_date)
  VALUES (100, 50, '1.0', '2015-05-07');